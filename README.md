# Overview
Run remote powershell commands or scripts from any system running python3

# Install
* `git clone https://actualnsnow@bitbucket.org/taoscloudteam/taos-scripting-winrmexec.git`
* `pip3 install pywinrm`

# Examples
## Command with args
* `./winrmExec.py us-west-wsus-1 ping "google.com,-4" -u taosengineering\\svc.winrm --ignorecert --log=debug`

## Script
* `./winrmExec.py us-west-wsus-1 test.ps1 -t s -u taosengineering\\svc.winrm --ignorecert --log=debug`

# Credits
* https://github.com/diyan/pywinrm

# Usage
```
usage: winrmExec.py [-h] [-u USER] [-p PASSWD] [-t TYPE] [--port PORT]
                    [--protocol {http,https}]
                    [--transport {ntlm,basic,plaintext,certificate,ssl,kerberos,credssp}]
                    [--encryption {auto,never,always}] [--ignorecert]
                    [--endpoint ENDPOINT]
                    [--log {debug,info,warning,error,critical}]
                    [--readtimeout READTIMEOUT]
                    [--operationtimeout OPERATIONTIMEOUT]
                    host command [args]

positional arguments:
  host                  Hostname for remote system
  command               Command to run on remote system
  args                  Command arguments (comma separated list of args, no
                        spaces)

optional arguments:
  -h, --help            show this help message and exit
  -u USER, --user USER  Username
  -p PASSWD, --passwd PASSWD
                        Password
  -t TYPE, --type TYPE  Command or Script
  --port PORT           WinRM port
  --protocol {http,https}
                        WinRM protocol
  --transport {ntlm,basic,plaintext,certificate,ssl,kerberos,credssp}
                        WinRM transport option
  --encryption {auto,never,always}
                        Message encryption option
  --ignorecert          Ignore server cert validation
  --endpoint ENDPOINT   Specify a custom endpoint url string
  --log {debug,info,warning,error,critical}
                        Log level
  --readtimeout READTIMEOUT
                        maximum seconds to wait before an HTTP connect/read
                        times out (default 30). This value should be slightly
                        higher than operation_timeout_sec, as the server can
                        block *at least* that long.
  --operationtimeout OPERATIONTIMEOUT
                        maximum allowed time in seconds for any single wsman
                        HTTP operation (default 20). Note that operation
                        timeouts while receiving output (the only wsman
                        operation that should take any significant time, and
                        where these timeouts are expected) will be silently
                        retried indefinitely.
```

