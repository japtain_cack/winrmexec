#!/usr/bin/env python3

import winrm
from winrm.protocol import Protocol
from getpass import getpass
from getpass import GetPassWarning
from pathlib import Path
from base64 import b64encode
import logging
import argparse
import re
import sys
import signal


parser = argparse.ArgumentParser()
parser.add_argument("host", help="Hostname for remote system", type=str)
parser.add_argument("command", help="Command to run on remote system", type=str)
parser.add_argument("args", help="Command arguments (comma separated list of args, no spaces)", type=str, nargs='?')
parser.add_argument("-u", "--user", help="Username", type=str)
parser.add_argument("-p", "--passwd", help="Password", type=str)
parser.add_argument("-t", "--type", help="Command or Script", type=str, default="c")
parser.add_argument("--port", help="WinRM port", type=str, default="5986")
parser.add_argument("--protocol", help="WinRM protocol", type=str, default="https", choices=["http", "https"])
parser.add_argument("--transport", help="WinRM transport option", type=str, default="ntlm", choices=["ntlm", "basic", "plaintext", "certificate", "ssl", "kerberos", "credssp"])
parser.add_argument("--encryption", help="Message encryption option", type=str, default="auto", choices=["auto", "never", "always"])
parser.add_argument("--ignorecert", help="Ignore server cert validation", action="store_true")
parser.add_argument("--endpoint", help="Specify a custom endpoint url string", type=str)
parser.add_argument("--log", help="Log level", default="warning", choices=["debug", "info", "warning", "error", "critical"])
#parser.add_argument("--catrustpath", help="Certification Authority trust path.")
#parser.add_argument("--certpem", help="client authentication certificate file path in PEM format")
#parser.add_argument("--certkeypem", help="client authentication certificate key file path in PEM format")
parser.add_argument("--readtimeout", default=30, help="maximum seconds to wait before an HTTP connect/read times out (default 30). This value should be slightly higher than operation_timeout_sec, as the server can block *at least* that long.")
parser.add_argument("--operationtimeout", default=20, help="maximum allowed time in seconds for any single wsman HTTP operation (default 20). Note that operation timeouts while receiving output (the only wsman operation that should take any significant time, and where these timeouts are expected) will be silently retried indefinitely.")
args = parser.parse_args()

# Set the log level and do some validation
numeric_level = getattr(logging, args.log.upper(), None)
try:
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % loglevel)
    logging.basicConfig(format='%(asctime)s %(levelname)s:%(message)s', level=numeric_level, datefmt='%d/%m/%Y %H:%M:%S')
    logger = logging.getLogger(__name__)
except ValueError as e:
    sys.exit("Unable to set log level: %s" % (str(e)))
except Exception as e:
    sys.exit("An unhandled exception occurred: %s" % (str(e)))


def main():
    passwd = ''
    ep = ''
    insecure = ''
    script = ''
    command = []
    cmd_args = []
    status_code = 1

    try:
        # Set the password if it was provided as an argument else prompt for a password
        passwd = args.passwd if args.passwd else getpass()

        # Generate the winrm endpoint if one isn't explicetly defined
        ep = args.endpoint if args.endpoint else args.protocol + '://' + args.host + ':' + args.port + '/wsman'

        # Determine if we are using a script or running a command and format the args if necessary
        if re.match('s|script', args.type.lower()):
            fpath = str(args.command)

            if not Path(fpath).is_file():
                raise Exception("File path is invalid or not a file")

            with open(fpath, 'r') as fh:
                script = fh.read()

                # Encode script so we can send it without being mangled
                encoded_ps = b64encode(script.encode('utf_16_le')).decode('ascii')

                # Build the command and insert the encoded script as an argument.
                command = "powershell"
                cmd_args = '-encodedcommand {0}'.format(encoded_ps).split(" ")

                # not sure how to pass args to an encoded script...
                #cmd_args += args.args.strip().split(",") if args.args else []

        else:
            # Get the command to be ran. Only the first item will be the command.
            command = args.command.strip().split(" ")[0] if args.command else []
            # Get the command args from a comma separated list
            cmd_args = args.args.strip().split(",") if args.args else []

        # Generate some info/debug messages to show what's happening
        logger.debug("User: %s", args.user)
        logger.info("Connecting to: %s", ep)
        logger.debug("Transport: %s", args.transport)
        logger.debug("Encryption: %s", args.encryption)
        logger.debug("Read timeout: %s", args.readtimeout)
        logger.debug("Operation timeout: %s", args.operationtimeout)
        logger.debug("Command: %s", command)
        logger.debug("Args: %s", cmd_args)

        # Show a warning if port/protocol don't seem to match
        if args.protocol == "http" and re.match('5986|443', args.port):
            logger.warning("Protocol is set to http but you seem to be using an https port: %s", args.port)
        if args.protocol == "https" and re.match('5985|80', args.port):
            logger.warning("Protocol is set to https but you seem to be using an http port: %s", args.port)

        # Show a warning if using insecure SSL mode
        if args.ignorecert:
            logger.warning("Ignoring certificate validation")

        # Bulid the winrm protocol object
        p = Protocol(
            endpoint = ep,
            transport = args.transport,
            username = args.user,
            password = passwd,
            message_encryption = args.encryption,
            server_cert_validation = "ignore" if args.ignorecert else None,
            read_timeout_sec = args.readtimeout,
            operation_timeout_sec = args.operationtimeout
        )

        # This block does the needful and runs a command/script
        shell_id = p.open_shell()
        command_id = p.run_command(shell_id, command, cmd_args)
        std_out, std_err, status_code = p.get_command_output(shell_id, command_id)
        p.cleanup_command(shell_id, command_id)
        p.close_shell(shell_id)
        std_out = std_out.decode("UTF-8")
        std_err = std_err.decode("UTF-8")

        # Show output from command/script
        if std_out:
            print(std_out)
        if '#< CLIXML' in std_err: #Suppress module import message
            pattern = re.compile('#< CLIXML.*Completed.*</SD></PR></MS></Obj></Objs>|<Obj S="progress".*?</Obj>', re.M|re.I|re.S)
            std_err = re.sub(pattern, '', std_err).strip()
        if std_err:
            raise PowershellError(std_err)
    except PowershellError as e:
        logger.critical("A powershell error was returned from the remote system: %s" % (str(e)))
        sys.exit(status_code)
    except GetPassWarning as e:
        logger.warning("Password input may be insecure: %s" % (str(e.error)))
    finally:
        print("Exit Code: %s" % status_code)
        sys.exit(status_code)

def signal_handler(sig, frame):
    sys.exit("Ctrl+c pressed. Exiting...")

class PowershellError(Exception):
    pass

if __name__ == '__main__':

    # Catch Ctrl+c keypress and exit gracefully
    signal.signal(signal.SIGINT, signal_handler)

    main()

